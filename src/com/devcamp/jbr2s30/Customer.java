package com.devcamp.jbr2s30;

public class Customer {
    private int Id = 0;
    private String name = "";
    private int discount = 0;
    
    public Customer(int id, String name, int discount) {
        Id = id;
        this.name = name;
        this.discount = discount;
    }

    public int getId() {
        return Id;
    }


    public String getName() {
        return name;
    }


    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Customer [Id = " + Id + ", name = " + name + ", % discount = " + discount + "]";
    }
}
