package com.devcamp.jbr2s30;

public class Invoice {
    int Id = 0;
    Customer customer;
    double amount = 0.0;
    
    public Invoice(int id, Customer customer, double amount) {
        Id = id;
        this.customer = customer;
        this.amount = amount;
    }

    public int getId() {
        return Id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerId(int id){
        return customer.getId();
    }

    public String getCustomerName(String name){
        return customer.getName();
    }

    public int getCustomerDiscount(int discount){
        return customer.getDiscount();
    }

    public double getAmountAfterDiscount(double amount){
        return amount*(100 - customer.getDiscount())/100;
    }

    @Override
    public String toString() {
        return "Invoice [Id = " + Id + ", customer = " + customer +  ", amount = " + amount + ", So con lai sau khi giam gia = " + this.getAmountAfterDiscount(amount) + "]";
    }
}
