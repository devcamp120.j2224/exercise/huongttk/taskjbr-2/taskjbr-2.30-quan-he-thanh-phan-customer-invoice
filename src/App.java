import com.devcamp.jbr2s30.Customer;
import com.devcamp.jbr2s30.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
       
        Customer customer1 = new Customer(11, "Duc", 10);
        Customer customer2 = new Customer(12, "Huong", 30);

        System.out.println("Customer1: " + customer1);
        System.out.println("Customer2: " + customer2);

        Invoice invoice1 = new Invoice(23, customer1, 5000.0);
        Invoice invoice2 = new Invoice(24, customer2, 4000.0);

        System.out.println("Invoice1: " + invoice1);
        System.out.println("Invoice2: " + invoice2);
    }
}
